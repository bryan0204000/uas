<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('name', 'desc')->paginate(10);

        return view('admin.users.index')->with([
            'users' => $users
        ]);
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $arr = [];

        $user = new User;
        $user->storeWithSync($request);
        return Redirect::back()->with('success', 'Data berhasil di tambahkan');
    }

    public function edit($id)
    {

        $user = User::find($id);
        return view('admin.users.edit')->with([
            'user' => $user
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->updateWithSync($request);

        return Redirect::back()->with('success', 'Data berhasil di update');

    }

    public function destroy($id)
    {
        $user = new User;
        $user = $user->find($id);
        $user->destroyWithSync();
        return Redirect::back()->with('success', 'Data berhasil di hapus');
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('admin.profile')->with([
            'user' => $user
        ]);
    }
}
