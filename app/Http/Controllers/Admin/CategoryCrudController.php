<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;


class CategoryCrudController extends Controller
{
    //List of all categories
    public function index()
    {
        $categories = Category::orderBy('publication_date', 'desc')->paginate(10);

        return view('admin.categories.index')->with([
            'categories' => $categories
        ]);
    }

    //Create Category
    public function create()
    {
        return view('admin.categories.create');
    }

    //Store Category
    public function store(Request $request)
    {
        $arr = [];

        $category = new Category;
        $arr['slug'] = Str::slug($request->input('name'));

        $category->storeWithSync($request, $arr);


        return Redirect::back()->with('success', 'Data berhasil di tambahkan');
    }

    //Edit Category
    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.categories.edit')->with([
            'category' => $category
        ]);
    }

    //Update Category
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $arr = [];

        $arr['slug'] = Str::slug($request->input('name'));
        $arr['articles'] = processRelatedArticles($category->articles);
        $category->updateWithSync($request, $arr);

        return Redirect::back()->with('success', 'Data berhasil di update');

    }

    //Destroy Category
    public function destroy($id)
    {
        // $category = Category::find($id);
        $category = new Category;

        $category = $category->find($id);

        $category->destroyWithSync();
        return Redirect::back()->with('success', 'Data berhasil di hapus');
    }
}




