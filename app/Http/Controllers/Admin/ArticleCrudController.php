<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use stdClass;
class ArticleCrudController extends Controller
{
    //List of all articles
    public function index()
    {
        $articles = Article::orderBy('publication_date', 'ASC')->paginate(10);

        return view('admin.articles.index')->with([
            'articles' => $articles
        ]);
    }


    //Create Article
    public function create()
    {
        $categories = Category::all();

        return view('admin.articles.create')->with([
            'categories' => $categories
        ]);
    }


    //Store Article
    public function store(Request $request)
    {
        $images = glob('images/blog/*');
        $fileName = $images[rand(0, count($images) - 1)];

        $arr = [];
        $article = new Article;
        $arr['autoincrement_id'] = getAID($article);
        $arr['author'] = auth()->user()->id;
        $arr['media_path'] = $fileName;
        $arr['slug'] = Str::slug($request->title) . '-' . $arr['autoincrement_id'];
        $arr['categories'] = $this->getCategories($request->categories);
        // $arr['primarycategory'] = $this->getPrimaryCategories($request->categories);
        $article->storeWithSync($request, $arr);
        return Redirect::back()->with('success', 'Data berhasil di tambahkan');
    }

    public function getCategories($categories_id)
    {
        $arr = [];
        $i = 0;


        if ($categories_id != null) {
            foreach ($categories_id as $category_id) {
                $category = Category::find($category_id);
                $newCategory = new stdClass();
                $newCategory->ref_id = $category->id;
                $newCategory->name = $category->name[cl()];
                $newCategory->slug = $category->slug[cl()];
                $newCategory->img_path = $category->img_path;
                $newCategory->description = $category->description[cl()];

                $arr[$i] = $newCategory;
                $i++;
            }
            return json_encode($arr);
        } else {
            return null;
        }
    }

    public function getPrimaryCategories($categories_id)
    {
        if ($categories_id != null && count($categories_id) > 0) {
            $category = Category::find($categories_id[0]);

            $newCategory = new stdClass();
            $newCategory->ref_id = $category->id;
            $newCategory->name = $category->name[cl()];
            $newCategory->description = $category->description[cl()];

            return json_encode($newCategory);
        }else {
            return null;
        }
    }


    //Edit Article
    public function edit($id)
    {
        $article = Article::find($id);
        $categories = Category::all();

        return view('admin.articles.edit')->with([
            'article' => $article,
            'categories' => $categories
        ]);
    }

    //Update Article
    public function update(Request $request, $id)
    {
        $article = Article::find($id);

        $arr = [];
        $arr['autoincrement_id'] = getAID( $article );
        $arr['slug'] = Str::slug($request->title) . '-' . $arr['autoincrement_id'];
        $arr['categories'] = $this->getCategories($request->categories);
        $article->updateWithSync($request, $arr);

        return Redirect::back()->with('success', 'Data berhasil di edit');
        
    }

    //Destroy Article
    public function destroy($id)
    {
        $article = Article::find($id);

        $article->destroyWithSync();

        return Redirect::back()->with('success', 'Data berhasil di hapus');
    }
}
