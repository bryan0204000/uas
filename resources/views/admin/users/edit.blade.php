@extends('layouts.backend')
@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
<form method="post" action="{{  route('admin.user-update', ['id' => $user->id]) }}">
    @csrf
    <div class="form-group">
        <label >Nama lengkap</label>
        <input value="{{ $user->surname }}" name="surname" type="text" class="form-control" />
    </div>
    <div class="form-group">
        <label >Username</label>
        <input value="{{ $user->name}}"  name="name" type="text" class="form-control" />
    </div>

    <div class="form-group">
        <label >Email</label>
        <input name="email" value="{{ $user->email }}" type="email" class="form-control" />
    </div>

    <div class="form-group">
        <label>Password</label>
        <input name="password" value="{{ $user->password }}" type="password" class="form-control" />
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
