@extends('layouts.backend')
@section('content')
@if($errors->any())
<h4>{{$errors->first()}}</h4>
@endif

@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

<table class="table">
    <thead>
    <tr>
        <th>No</th>
        <th>id</th>
        <th>Name</th>
        <th colspan="2"><center>Aksi</center></th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
    <tr>
        <td>{{ $loop-> index + 1}}</td>
        <td>{{ $user->id}}</td>
        <td>{{ $user->name}}</td>
        <!-- <td><a href="{{ route('admin.user-edit', ['id' => $user->id]) }}">Edit</a></td> -->
        <td><a href="{{ route('admin.user-destroy', ['id' => $user->id]) }}">Delete</a></td>
    </tr>
    @endforeach
    </tbody>
    <tbody>
</table>

<div class="d-flex justify-content-center">
    {!! $users->links() !!}
</div>
@endsection
