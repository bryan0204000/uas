@extends('layouts.auth')

@section('content')
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block" style="
                            background: url({{ asset('images/dog.jpeg') }});
                            background-position: center;
                            background-size: cover;
                        "></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <form class="user" method="POST" action="{{ route('login') }}">
                                    <div class="form-group">
                                        <input type="text"
                                               name="email"
                                               disabled
                                               value="{{$user->surname}}"
                                               class="form-control form-control-user  @error('email') is-invalid @enderror"
                                               id="exampleInputEmail" aria-describedby="emailHelp"
                                               placeholder="Enter Email Address..." value="{{ old('email') }}"
                                               autocomplete="email" autofocus>

                                    </div>
                                    <div class="form-group">
                                        <input type="text"
                                               name="email"
                                               disabled
                                               value="{{$user->name}}"
                                               class="form-control form-control-user  @error('email') is-invalid @enderror"
                                               id="exampleInputEmail" aria-describedby="emailHelp"
                                               placeholder="Enter Email Address..." value="{{ old('email') }}"
                                               autocomplete="email" autofocus>

                                    </div>
                                    <div class="form-group">
                                        <input type="email"
                                               value="{{$user->email}}"
                                               disabled
                                               class="form-control form-control-user @error('password') is-invalid @enderror"
                                               id="exampleInputPassword" placeholder="Password" name="password"
                                               autocomplete="current-password">
                                    </div>
                                    <a class="btn btn-primary btn-user btn-block" href="{{ route('logout') }}">
                                            Logout
                                    </a>
                                </form>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
