@extends('layouts.backend')
@section('content')
@if($errors->any())
<h4>{{$errors->first()}}</h4>
@endif

@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

<table class="table">
    <thead>
    <tr>
        <th>No</th>
        <th>id</th>
        <th>Name</th>
        <th colspan="2"><center>Aksi</center></th>
    </tr>
    </thead>
    <tbody>
    @foreach($categories as $value)
    <tr>
        <td>{{ $loop-> index + 1}}</td>
        <td>{{ $value->id}}</td>
        <td>{{ $value->name['en']}}</td>
        <td><a href="{{ route('admin.category-edit', ['id' => $value->id]) }}">Edit</a></td>
        <td><a href="{{ route('admin.category-destroy', ['id' => $value->id]) }}">Delete</a></td>
    </tr>
    @endforeach
    </tbody>
    <tbody>
</table>

<div class="d-flex justify-content-center">
    {!! $categories->links() !!}
</div>
@endsection
