@extends('layouts.backend')
@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
<form method="post" action="{{ route('admin.category-update', ['id' => $category->id]) }}">
    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Name</label>
        <input name="name" value="{{ $category->name['en']}}" type="text" class="form-control" />
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Slug</label>
        <input name="slug" value="{{ $category->slug['en']}}" type="text" class="form-control" />
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Description</label>
        <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3">{{ $category->description['en']}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
