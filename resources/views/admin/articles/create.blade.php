@extends('layouts.backend')
@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
<form method="post" action="{{ route('admin.article-store') }}">
    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Title</label>
        <input name="title" type="text" class="form-control" />
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Excerpt</label>
        <input name="excerpt" type="text" class="form-control" />
    </div>
    <!-- <div class="form-group">
        <label for="exampleInputEmail1">Slug</label>
        <input name="slug" type="text" class="form-control" />
        <input name="publication_date" type="date" class="form-control" />
    </div> -->
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Content</label>
        <textarea name="content" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>

    <div class="form-group">
        <label>Category</label>
        <div class=" p-0">
            <select class="col-md-4" id="category" name="categories[]" class="form-group " multiple>
                @foreach($categories as $value)
                    <option value="{{ $value->id}}">{{ $value->name['en']}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<script src="{{ asset('js/admin.js') }}"></script>
<script src="{{ asset('js/multi-select.js') }}"></script>

<script type="text/javascript">
    // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('#category').select2();
});
</script>
@endsection
